# Shared Drives List

List all shared drives using Google Drive API, alternative to view all your drives because Google Drive App doesn't show all most of the time.


## Getting started

* Make a Worker
* Make a KV Namespace
* Go to Worker Settings
* Go to Variable Settings
* Add KV Namespace Bindings
* Set Variable Name as ENV
* Bind it
* Generate Refresh Token from https://bdi-generator.hashhackers.com First
* Copy workers.js Code
* Paste Refresh Token or Use Service Account
* Deploy and Open Site

## Authors and acknowledgment

* Written by Parveen Bhadoo
* Co-Authored by ChatGPT

## License

* MIT
